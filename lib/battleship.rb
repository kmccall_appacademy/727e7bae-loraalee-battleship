require_relative "board"
require_relative "player"

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new, board = Board.new)
    @player = player
    @board = board
  end

  def attack(pos)
    x = pos[0]
    y = pos[1]

    if board.grid[x][y] == nil
      puts "You attacked an empty space!"
      board.grid[x][y] = :x
    elsif board.grid[x][y] == :s
      puts "You attacked a ship!"
      board.grid[x][y] = :x
    else
      puts "You already attacked that space!"
    end
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end
end

if $PROGRAM_NAME == __FILE__
  BattleshipGame.new.play
end
