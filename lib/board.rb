class Board
  attr_reader :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def count
    count = 0

    grid.each do |row|
      row.each do |space|
        count += 1 if space == :s
      end
    end

    count
  end

  def [](pos)
    grid[pos.first][pos.last]
  end

  def empty?(pos=[])
    if pos != []
      if grid[pos.first][pos.last] == nil
        true
      else
        false
      end
    else
      if self.count == 0
        true
      else
        false
      end
    end
  end

  def full?
    grid.each do |row|
      row.each do |space|
        return false if space == nil
      end
    end

    true
  end

  def place_random_ship
    num_rows = grid.length
    num_cols = grid[0].length

    if self.full?
      raises error
    elsif self.empty?
      rand_row = rand(num_rows)
      rand_col = rand(num_cols)
      grid[rand_row][rand_col] = :s
    else
      while !self.full?
        rand_row = rand(num_rows)
        rand_col = rand(num_cols)
        grid[rand_row][rand_col] = :s
      end
    end
  end

  def won?
    if empty?
      true
    else
      false
    end
  end
end
