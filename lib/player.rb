class HumanPlayer
  attr_reader :name

  def initialize(name = "Player")
    @name = name
  end

  def get_play
    puts "Enter a position. Valid positions: 0 to #{board.grid.length - 1}"
    gets.chomp
  end
end
